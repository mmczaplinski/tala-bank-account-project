# Bank Account 

## Background 

This implements a simple microservice with an API that models a bank account.
The backend storage is using the embedded H2 database.

## Installation

Assuming that you have already cloned the project:
```bash
cd tala-bank-account-project
```

## Run the project:
```bash
./gradlew bootRun
```

## Run the tests:
```bash
./gradlew test

```

## Some assumptions made while implementing the project

1. The program is using the embedded H2 database as storage. 
A database is created new on every application startup, and the history of users's transactions is not persisted across applications startups.
The reason for this is so that that application can be run without setting up a separate DB server, but on the downside, it does not of course, reflect a "real" use case.

2. One is able to withdraw integer amounts of money, which is probably not "realistic" since in most countries one can withdraw only multiples of 10 or 100.
