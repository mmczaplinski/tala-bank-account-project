package com.tala.talabankaccount.account;

import com.tala.talabankaccount.account.AccountErrors.AccountException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

@Service
public class AccountService {

    @Autowired private JdbcTemplate jdbcTemplate;

    Integer balance() {
        AccountRepository accountRepository = new AccountRepository(jdbcTemplate);
        return accountRepository.balance();
    }

    void deposit(Integer amount) throws AccountException {

        AccountRepository accountRepository = new AccountRepository(jdbcTemplate);

        if (amount > AccountConfiguration.MAX_DEPOSIT_PER_TRANSACTION) {
            throw new AccountException(AccountErrors.EXCEEDS_MAX_DEPOSIT_PER_TRANSACTION);
        }

        if ((accountRepository.amountDepositedToday() + amount) > AccountConfiguration.MAX_DEPOSIT_PER_DAY) {
            throw new AccountException(AccountErrors.EXCEEDS_MAX_DEPOSIT_PER_DAY);
        }

        if (accountRepository.numberOfDepositsToday() >= AccountConfiguration.MAX_NUM_DEPOSITS_PER_DAY) {
            throw new AccountException(AccountErrors.EXCEEDS_MAX_NUM_DEPOSITS_PER_DAY);
        }

        accountRepository.deposit(amount);
    }

    void withdraw(Integer amount) throws AccountException{

        AccountRepository accountRepository = new AccountRepository(jdbcTemplate);

        if (amount > AccountConfiguration.MAX_WITHDRAWAL_PER_TRANSACTION) {
            throw new AccountException(AccountErrors.EXCEEDS_MAX_WITHDRAWAL_PER_TRANSACTION);
        }

        if ((accountRepository.amountWithdrawnToday()) + amount > AccountConfiguration.MAX_WITHDRAWAL_PER_DAY) {
            throw new AccountException(AccountErrors.EXCEEDS_MAX_WITHDRAWAL_PER_DAY);
        }

        if (accountRepository.numberOfWithdrawalsToday() >= AccountConfiguration.MAX_NUM_WITHDRAWALS_PER_DAY) {
            throw new AccountException(AccountErrors.EXCEEDS_MAX_NUM_WITHDRAWALS_PER_DAY);
        }

        if (amount > accountRepository.balance()) {
            throw new AccountException(AccountErrors.EXCEEDS_ACCOUNT_BALANCE);
        }

        accountRepository.withdraw(amount);
    }

}
