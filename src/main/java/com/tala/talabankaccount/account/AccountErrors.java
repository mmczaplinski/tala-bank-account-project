package com.tala.talabankaccount.account;

class AccountErrors {

    static final String EXCEEDS_MAX_DEPOSIT_PER_TRANSACTION = "Exceeded max deposit per transaction";
    static final String EXCEEDS_MAX_DEPOSIT_PER_DAY = "Exceeded max daily deposit amount";
    static final String EXCEEDS_MAX_NUM_DEPOSITS_PER_DAY = "Exceeded max daily number of deposits";

    static final String EXCEEDS_MAX_WITHDRAWAL_PER_TRANSACTION = "Exceeded max withdrawal per transaction";
    static final String EXCEEDS_MAX_WITHDRAWAL_PER_DAY = "Exceeded max daily withdrawal amount";
    static final String EXCEEDS_MAX_NUM_WITHDRAWALS_PER_DAY = "Exceeded max daily number of withdrawals";
    static final String EXCEEDS_ACCOUNT_BALANCE = "Exceeded available account balance";

    public static class AccountException extends Exception {

        public AccountException(String message) {
            super(message);
        }

        public AccountException(String message, Throwable throwable) {
            super(message, throwable);
        }

    }
}
