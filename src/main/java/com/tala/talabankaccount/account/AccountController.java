package com.tala.talabankaccount.account;

import com.tala.talabankaccount.account.AccountErrors.AccountException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class AccountController {

    private class Response {
        private final Integer status;
        private final String message;

        public Response(Integer status, String message) {
            this.status = status;
            this.message = message;
        }

        public Integer getStatus() {
            return status;
        }

        public String getMessage() {
            return message;
        }
    }

    private class BalanceResponse extends Response {
        private final Integer balance;

        public BalanceResponse(Integer status, String message, Integer balance) {
            super(status, message);
            this.balance = balance;
        }

        public Integer getBalance() {
            return balance;
        }
    }

    @Autowired
    AccountService accountService;

    @RequestMapping(value="/balance", method=RequestMethod.GET)
    ResponseEntity balance() {
        Integer balance = accountService.balance();
        return ResponseEntity
                .status(HttpStatus.OK)
                .body(new BalanceResponse(200, "200 OK", balance));
    }

    @RequestMapping(value="/deposit/{amount:[\\d]+}", method=RequestMethod.POST)
    ResponseEntity deposit(@PathVariable Integer amount) {
        try {
            accountService.deposit(amount);
        } catch (AccountException e) {
            switch (e.getMessage()) {
                case AccountErrors.EXCEEDS_MAX_DEPOSIT_PER_TRANSACTION:
                    return ResponseEntity
                            .status(HttpStatus.FORBIDDEN)
                            .body(new Response(403, "Exceeded maximum deposit per transaction"));
                case AccountErrors.EXCEEDS_MAX_DEPOSIT_PER_DAY:
                    return ResponseEntity
                            .status(HttpStatus.FORBIDDEN)
                            .body(new Response(403, "Exceeded maximum deposit per day"));
                case AccountErrors.EXCEEDS_MAX_NUM_DEPOSITS_PER_DAY:
                    return ResponseEntity
                            .status(HttpStatus.FORBIDDEN)
                            .body(new Response(403, "Exceeded maximum number of deposits per day"));
            }
        }

        return ResponseEntity
                .status(HttpStatus.OK)
                .body(new Response(200, "200 OK"));
    }

    @RequestMapping(value="/withdraw/{amount:[\\d]+}", method=RequestMethod.POST)
    ResponseEntity withdraw(@PathVariable Integer amount) {
        try {
            accountService.withdraw(amount);
        } catch (AccountException e) {
            switch (e.getMessage()) {
                case AccountErrors.EXCEEDS_MAX_WITHDRAWAL_PER_TRANSACTION:
                     return ResponseEntity
                        .status(HttpStatus.FORBIDDEN)
                        .body(new Response(403, "Exceeded maximum withdrawal per transaction"));
                case AccountErrors.EXCEEDS_MAX_WITHDRAWAL_PER_DAY:
                    return ResponseEntity
                        .status(HttpStatus.FORBIDDEN)
                        .body(new Response(403, "Exceeded maximum withdrawal per day"));
                case AccountErrors.EXCEEDS_MAX_NUM_WITHDRAWALS_PER_DAY:
                    return ResponseEntity
                            .status(HttpStatus.FORBIDDEN)
                            .body(new Response(403, "Exceeded maximum number of withdrawals per day"));
                case AccountErrors.EXCEEDS_ACCOUNT_BALANCE:
                    return ResponseEntity
                            .status(HttpStatus.FORBIDDEN)
                            .body(new Response(403, "Exceeded your account balance"));
            }
        }

        return ResponseEntity
                .status(HttpStatus.OK)
                .body(new Response(200, "200 OK"));
    }


    public static void main(String[] args) throws Exception {
        SpringApplication.run(AccountController.class, args);
    }
}