package com.tala.talabankaccount.account;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.sql.Date;
import java.time.LocalDate;

@Repository
public class AccountRepository {

    private final JdbcTemplate jdbcTemplate;

    Integer balance() {
        String sql = "SELECT sum(amount) FROM transactions";
        return this.jdbcTemplate.queryForObject(sql, Integer.class);
    }

    void deposit(Integer amount) {
        Date today = Date.valueOf(LocalDate.now());
        jdbcTemplate.update("INSERT INTO transactions VALUES (?, 'deposit', ?)", today, amount);
    }

    void withdraw(Integer amount) {
        Date today = Date.valueOf(LocalDate.now());
        jdbcTemplate.update("INSERT INTO transactions VALUES (?, 'withdrawal', ?)", today, -amount);
    }


    Integer amountDepositedToday() {
        Date today = Date.valueOf(LocalDate.now());
        String sql = "SELECT sum(amount) FROM transactions WHERE transaction_date=? AND transaction_type=?";
        Integer depositedToday = this.jdbcTemplate.queryForObject(sql, Integer.class, today, "deposit");

        // if there was no deposit made today yet, set the amount to 0;
        if (depositedToday == null) depositedToday = 0;
        return depositedToday;
    }

    Integer numberOfDepositsToday() {
        Date today = Date.valueOf(LocalDate.now());
        String sql = "SELECT count(*) FROM transactions WHERE transaction_date=? AND transaction_type=?";
        return this.jdbcTemplate.queryForObject(sql, Integer.class, today, "deposit");
    }

    Integer amountWithdrawnToday() {
        Date today = Date.valueOf(LocalDate.now());
        String sql = "SELECT abs(sum(amount)) FROM transactions WHERE transaction_date=? AND transaction_type=?";
        Integer withdrawnToday = this.jdbcTemplate.queryForObject(sql, Integer.class, today, "withdrawal");

        // if there was no withdrawal made today yet, set the amount to 0;
        if (withdrawnToday == null) withdrawnToday = 0;
        return withdrawnToday;
    }

    Integer numberOfWithdrawalsToday() {
        Date today = Date.valueOf(LocalDate.now());
        String sql = "SELECT count(*) FROM transactions WHERE transaction_date=? AND transaction_type=?";
        return this.jdbcTemplate.queryForObject(sql, Integer.class, today, "withdrawal");
    }

    public AccountRepository(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }
}
