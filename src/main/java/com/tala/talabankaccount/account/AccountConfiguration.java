package com.tala.talabankaccount.account;

public class AccountConfiguration {
    static final Integer MAX_DEPOSIT_PER_DAY = 150000;
    static final Integer MAX_DEPOSIT_PER_TRANSACTION = 40000;
    static final Integer MAX_NUM_DEPOSITS_PER_DAY = 4;

    static final Integer MAX_WITHDRAWAL_PER_DAY = 50000;
    static final Integer MAX_WITHDRAWAL_PER_TRANSACTION = 20000;
    static final Integer MAX_NUM_WITHDRAWALS_PER_DAY = 3;
}
