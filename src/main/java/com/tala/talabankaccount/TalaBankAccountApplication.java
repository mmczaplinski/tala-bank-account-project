package com.tala.talabankaccount;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.jdbc.core.JdbcTemplate;

import java.sql.Date;
import java.time.LocalDate;

@SpringBootApplication
public class TalaBankAccountApplication implements CommandLineRunner {

    private static final Logger log = LoggerFactory.getLogger(TalaBankAccountApplication.class);

    public static void main(String[] args) throws Exception {
        SpringApplication.run(TalaBankAccountApplication.class, args);
    }

    @Autowired
    JdbcTemplate jdbcTemplate;

    @Override
    public void run(String... strings) throws Exception {

        log.info("Creating tables");

        Date today = Date.valueOf(LocalDate.now());

        jdbcTemplate.execute("DROP TABLE IF EXISTS transactions");
        jdbcTemplate.execute("CREATE TABLE transactions(transaction_date DATE, transaction_type ENUM('deposit', 'withdrawal'), amount BIGINT)");
        jdbcTemplate.update("INSERT INTO transactions VALUES (?, 'deposit', 0)", today);
    }
}
