package com.tala.talabankaccount;

import org.springframework.jdbc.core.JdbcTemplate;

public class TestUtils {

    private final JdbcTemplate jdbcTemplate;

    public void initalizeDatabase() {
        this.jdbcTemplate.execute("DROP TABLE IF EXISTS transactions");
        this.jdbcTemplate.execute("CREATE TABLE transactions(transaction_date DATE, transaction_type ENUM('deposit', 'withdrawal'), amount BIGINT)");
        this.jdbcTemplate.update("INSERT INTO transactions VALUES (DATE '2017-09-28', 'deposit', 0)");
    }

    public TestUtils(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }
}
