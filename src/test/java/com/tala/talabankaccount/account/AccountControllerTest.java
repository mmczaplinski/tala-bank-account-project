package com.tala.talabankaccount.account;

import com.tala.talabankaccount.TestUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


@RunWith(SpringRunner.class)
@WebAppConfiguration
@SpringBootTest
public class AccountControllerTest {

    @Autowired
    private WebApplicationContext wac;

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Autowired
    private AccountService accountService;

    private MockMvc mockMvc;

    @Before
    public void setup() throws Exception {
        TestUtils testUtils = new TestUtils(jdbcTemplate);
        testUtils.initalizeDatabase();

        this.mockMvc = MockMvcBuilders.webAppContextSetup(this.wac).build();
    }


    @Test
    public void balance() throws Exception {
        mockMvc.perform(get("/balance"))
                .andExpect(status().isOk())
                .andExpect(content().json("{\"status\": 200, \"message\": \"200 OK\", \"balance\": 0}"));
    }


    @Test
    public void depositSuccesfully() throws Exception {
        mockMvc.perform(post("/deposit/10"))
                .andExpect(status().isOk())
                .andExpect(content().json("{\"status\": 200, \"message\": \"200 OK\"}"));
    }

    @Test
    public void depositExceedMaximumPerTransaction() throws Exception {
        mockMvc.perform(post("/deposit/40001"))
                .andExpect(status().isForbidden())
                .andExpect(content().json("{\"status\": 403, \"message\": \"Exceeded maximum deposit per transaction\"}" ));
    }

    @Test
    public void depositExceedMaximumPerDay() throws Exception {
        accountService.deposit(40000);
        accountService.deposit(40000);
        accountService.deposit(40000);

        mockMvc.perform(post("/deposit/30001"))
                .andExpect(status().isForbidden())
                .andExpect(content().json("{\"status\": 403, \"message\": \"Exceeded maximum deposit per day\"}" ));
    }

    @Test
    public void depositExceedMaximumNumberPerDay() throws Exception {
        mockMvc.perform(post("/deposit/10"))
                .andExpect(status().isOk())
                .andExpect(content().json("{\"status\": 200, \"message\": \"200 OK\"}"));
        mockMvc.perform(post("/deposit/10"))
                .andExpect(status().isOk())
                .andExpect(content().json("{\"status\": 200, \"message\": \"200 OK\"}"));
        mockMvc.perform(post("/deposit/10"))
                .andExpect(status().isOk())
                .andExpect(content().json("{\"status\": 200, \"message\": \"200 OK\"}"));
        mockMvc.perform(post("/deposit/10"))
                .andExpect(status().isOk())
                .andExpect(content().json("{\"status\": 200, \"message\": \"200 OK\"}"));
        mockMvc.perform(post("/deposit/10"))
                .andExpect(status().isForbidden())
                .andExpect(content().json("{\"status\": 403, \"message\": \"Exceeded maximum number of deposits per day\"}"));
    }

    @Test
    public void withdrawSuccessfully() throws Exception {
        accountService.deposit(10);

        mockMvc.perform(post("/withdraw/10"))
                .andExpect(status().isOk())
                .andExpect(content().json("{\"status\": 200, \"message\": \"200 OK\"}"));
    }


    @Test
    public void withdrawalExceedMaximumPerTransaction() throws Exception {
        accountService.deposit(40000);

        mockMvc.perform(post("/withdraw/20001"))
                .andExpect(status().isForbidden())
                .andExpect(content().json("{\"status\": 403, \"message\": \"Exceeded maximum withdrawal per transaction\"}" ));
    }

    @Test
    public void withdrawalExceedMaximumPerDay() throws Exception {
        accountService.deposit(40000);
        accountService.deposit(40000);
        accountService.withdraw(20000);
        accountService.withdraw(20000);

        mockMvc.perform(post("/withdraw/10001"))
                .andExpect(status().isForbidden())
                .andExpect(content().json("{\"status\": 403, \"message\": \"Exceeded maximum withdrawal per day\"}" ));
    }

    @Test
    public void withdrawalExceedMaximumNumberPerDay() throws Exception {
        accountService.deposit(10000);

        mockMvc.perform(post("/withdraw/10"))
                .andExpect(status().isOk())
                .andExpect(content().json("{\"status\": 200, \"message\": \"200 OK\"}"));
        mockMvc.perform(post("/withdraw/10"))
                .andExpect(status().isOk())
                .andExpect(content().json("{\"status\": 200, \"message\": \"200 OK\"}"));
        mockMvc.perform(post("/withdraw/10"))
                .andExpect(status().isOk())
                .andExpect(content().json("{\"status\": 200, \"message\": \"200 OK\"}"));
        mockMvc.perform(post("/withdraw/10"))
                .andExpect(status().isForbidden())
                .andExpect(content().json("{\"status\": 403, \"message\": \"Exceeded maximum number of withdrawals per day\"}"));
    }
}