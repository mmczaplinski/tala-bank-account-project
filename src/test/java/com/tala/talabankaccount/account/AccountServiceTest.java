package com.tala.talabankaccount.account;

import com.tala.talabankaccount.TestUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.junit4.SpringRunner;

import com.tala.talabankaccount.account.AccountErrors.AccountException;
import java.sql.Date;
import java.time.LocalDate;

import static org.junit.Assert.*;

interface BalanceTests {}
interface DepositTests {}
interface WithdrawalTests {}

@RunWith(SpringRunner.class)
@SpringBootTest
public class AccountServiceTest {

    @Autowired
    private JdbcTemplate jdbcTemplate;
    @Autowired
    private AccountService accountService;

    @Before
    public void setUp() {
        TestUtils testUtils = new TestUtils(jdbcTemplate);
        testUtils.initalizeDatabase();
    }

    @Test
    @Category(BalanceTests.class)
    public void initialBalance() {
        assertSame(accountService.balance(), 0);
    }

    @Test
    @Category(BalanceTests.class)
    public void updatedBalance() {
        Date today = Date.valueOf(LocalDate.now());
        this.jdbcTemplate.update("INSERT INTO transactions VALUES (?, 'deposit', 20)", today);

        assertSame(accountService.balance(), 20);
    }

    @Test
    @Category(DepositTests.class)
    public void depositSuccessful() throws AccountException {
        accountService.deposit(10);

        assertSame(accountService.balance(), 10);
    }

    @Test
    @Category(DepositTests.class)
    public void exceedDepositAmount() throws AccountException {
        try {
            accountService.deposit(150001);
            assertFalse(
                    String.format("The exception '%s' was not thrown", AccountErrors.EXCEEDS_MAX_DEPOSIT_PER_TRANSACTION),
                    true
            );
        } catch (AccountException e) {
            assertEquals(e.getMessage(), AccountErrors.EXCEEDS_MAX_DEPOSIT_PER_TRANSACTION);
        }
    }

    @Test
    @Category(DepositTests.class)
    public void exceedDailyDepositAmount() throws AccountException {

        // deposit a total of 120K in 3 separate transactions so as not to exceed the per transaction limit.
        accountService.deposit(40000);
        accountService.deposit(40000);
        accountService.deposit(40000);

        try {
            accountService.deposit(30001);
            assertFalse(
                    String.format("The exception '%s' was not thrown", AccountErrors.EXCEEDS_MAX_DEPOSIT_PER_DAY),
                    true
            );
        } catch (AccountException e) {
            assertEquals(e.getMessage(), AccountErrors.EXCEEDS_MAX_DEPOSIT_PER_DAY);
        }
    }

    @Test
    @Category(DepositTests.class)
    public void exceedDailyDepositFrequency() throws AccountException {

        accountService.deposit(10);
        accountService.deposit(10);
        accountService.deposit(10);
        accountService.deposit(10);

        try {
            accountService.deposit(10);
            assertFalse(
                    String.format("The exception '%s' was not thrown", AccountErrors.EXCEEDS_MAX_NUM_DEPOSITS_PER_DAY),
                    true
            );
        } catch (AccountException e) {
            assertEquals(e.getMessage(), AccountErrors.EXCEEDS_MAX_NUM_DEPOSITS_PER_DAY);
        }
    }


    @Test
    @Category(WithdrawalTests.class)
    public void withdrawalSuccessful() throws AccountException {
        Date today = Date.valueOf(LocalDate.now());
        this.jdbcTemplate.update("INSERT INTO transactions VALUES (?, 'deposit', 30)", today);

        accountService.withdraw(20);

        assertSame(accountService.balance(), 10);
    }

    @Test
    @Category(WithdrawalTests.class)
    public void exceedWithdrawalAmount() throws AccountException {

        Date today = Date.valueOf(LocalDate.now());
        this.jdbcTemplate.update("INSERT INTO transactions VALUES (?, 'deposit', 100000)", today);

        try {
            accountService.withdraw(20001);
            assertFalse(
                    String.format("The exception '%s' was not thrown", AccountErrors.EXCEEDS_MAX_WITHDRAWAL_PER_TRANSACTION),
                    true
            );
        } catch (AccountException e) {
            assertEquals(e.getMessage(), AccountErrors.EXCEEDS_MAX_WITHDRAWAL_PER_TRANSACTION);
        }
    }

    @Test
    @Category(WithdrawalTests.class)
    public void exceedDailyWithdrawalAmount() throws AccountException {

        Date today = Date.valueOf(LocalDate.now());
        this.jdbcTemplate.update("INSERT INTO transactions VALUES (?, 'deposit', 100000)", today);

        // withdraw a total of 50k in 3 separate transactions so as not to exceed the per transaction limit.
        accountService.withdraw(20000);
        accountService.withdraw(20000);

        try {
            accountService.withdraw(10001);
            assertFalse(
                    String.format("The exception '%s' was not thrown", AccountErrors.EXCEEDS_MAX_WITHDRAWAL_PER_DAY),
                    true
            );
        } catch (AccountException e) {
            assertEquals(e.getMessage(), AccountErrors.EXCEEDS_MAX_WITHDRAWAL_PER_DAY);
        }
    }

    @Test
    @Category(WithdrawalTests.class)
    public void exceedDailyWithdrawalFrequency() throws AccountException {

        Date today = Date.valueOf(LocalDate.now());
        this.jdbcTemplate.update("INSERT INTO transactions VALUES (?, 'deposit', 100000)", today);

        accountService.withdraw(10);
        accountService.withdraw(10);
        accountService.withdraw(10);

        try {
            accountService.withdraw(10);
            assertFalse(
                    String.format("The exception '%s' was not thrown", AccountErrors.EXCEEDS_MAX_NUM_WITHDRAWALS_PER_DAY),
                    true
            );
        } catch (AccountException e) {
            assertEquals(e.getMessage(), AccountErrors.EXCEEDS_MAX_NUM_WITHDRAWALS_PER_DAY);
        }
    }

    @Test
    @Category(WithdrawalTests.class)
    public void exceedAccountBalance() throws AccountException {

        Date today = Date.valueOf(LocalDate.now());
        this.jdbcTemplate.update("INSERT INTO transactions VALUES (?, 'deposit', 10)", today);

        try {
            accountService.withdraw(20);
            assertFalse(
                    String.format("The exception '%s' was not thrown", AccountErrors.EXCEEDS_ACCOUNT_BALANCE),
                    true
            );
        } catch (AccountException e) {
            assertEquals(e.getMessage(), AccountErrors.EXCEEDS_ACCOUNT_BALANCE);
        }
    }


}