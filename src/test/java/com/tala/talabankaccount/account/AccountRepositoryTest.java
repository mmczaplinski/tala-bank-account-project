package com.tala.talabankaccount.account;

import com.tala.talabankaccount.TestUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.junit4.SpringRunner;

import java.sql.Date;
import java.time.LocalDate;

import static org.junit.Assert.assertSame;


@RunWith(SpringRunner.class)
@SpringBootTest
public class AccountRepositoryTest {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Autowired
    private AccountRepository accountRepository;

    @Before
    public void setUp() {
        TestUtils testUtils = new TestUtils(jdbcTemplate);
        testUtils.initalizeDatabase();
        Date today = Date.valueOf(LocalDate.now());
        this.jdbcTemplate.update("INSERT INTO transactions VALUES (?, 'deposit', 20)", today);
        this.jdbcTemplate.update("INSERT INTO transactions VALUES (?, 'withdrawal', -10)", today);
        this.jdbcTemplate.update("INSERT INTO transactions VALUES (?, 'deposit', 30)", today);
        this.jdbcTemplate.update("INSERT INTO transactions VALUES (?, 'withdrawal', -20)", today);
        this.jdbcTemplate.update("INSERT INTO transactions VALUES (?, 'deposit', 40)", today);
    }

    @Test
    public void balance() {
        assertSame(accountRepository.balance(), 60);
    }

    @Test
    public void depositedToday() {
        assertSame(accountRepository.amountDepositedToday(), 90);
    }

    @Test
    public void numberOfDepositsToday() {
        assertSame(accountRepository.numberOfDepositsToday(), 3);
    }

    @Test
    public void withdrawnToday() {
        assertSame(accountRepository.amountWithdrawnToday(), 30);
    }

    @Test
    public void numberOfWithdrawalsToday() {
        assertSame(accountRepository.numberOfWithdrawalsToday(), 2);
    }

}